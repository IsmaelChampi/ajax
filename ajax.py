from flask import Flask, jsonify, render_template, request
import json
app = Flask(__name__)

@app.route('/_add_numbers', methods=['POST'])
def add_numbers():
  if request.method == 'POST':
    """modify/update the information for <user_id>"""
    data = request.get_json()
    add = int(data['a']) + int(data['b'])
    print (add)

    return jsonify(result=add)


@app.route('/')
def home():
    return render_template('ajax.html')


if __name__ == '__main__':
    app.run(debug=True)